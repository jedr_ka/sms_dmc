//
// Created by jedrek on 2017-12-10.
//

#ifndef UNTITLED_FLOATDEBUG_H
#define UNTITLED_FLOATDEBUG_H

#include <stdio.h>

typedef union {
    float f;
    struct {
        unsigned int mantisa : 23;
        unsigned int exponent : 8;
        unsigned int sign : 1;
    } parts;
} float_cast;


void debug_float(float *f);

#endif //UNTITLED_FLOATDEBUG_H
