//
// Created by jedrek on 2017-12-12.
//
#include "pid.h"


static float Ku = 30, Tu = 0.38f;//K krytyczne i okres

static float K = 28.f;
static float Ti = 1000000.f;//+inf
static float Td = 0.0;
static float T = 0.05;//czas próbkowania/regulacji

void pid_init() {

    //PID
    /*
    K = 0.6f*Ku;
    Ti = Tu/2.0f;
	Td = Tu/8.0f;
    */

    //PI
    /*
    K = 0.45f*Ku;
    Ti = Tu / 1.2f;
    */

    //PI
    K = 0.01f * Ku;
    Ti = Tu / 1.1f;

}

static float e;
static float e_old = 0.0f;

static float up = 0.0;
static float ui = 0.0;
static float ud = 0.0;

static float uw_old = 0.0f;
static float u_old = 0.0f;

void pid_see(float *re, float *rup, float *rui, float *rud) {
    if (re != NULL) {
        *re = e;
    }
    if (rup != NULL) {
        *rup = up;
    }
    if (rui != NULL) {
        *rui = ui;
    }
    if (rud != NULL) {
        *rud = ud;
    }
}

float pid_go(float y, float yzad, float u_minmax, float Tv) {

    float u;

    e = yzad - y;

    up = K * e;
    ui = ui + (K / Ti * T) * ((e_old + e_old) / 2) + (T / Tv * (uw_old - u_old));//+==windup
    ud = K * Td * (e - e_old) / T;

    //PID
    u = up + ui + ud;

    //PI
    //u = up + ui;

    u_old = u;

    if (u < -u_minmax) {
        u = -u_minmax;
    } else if (u > u_minmax) {
        u = u_minmax;
    }

    uw_old = u;

    e_old = e;

    return u;
}