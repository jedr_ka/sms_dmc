//
// Created by jedrek on 2017-12-10.
//

#include "dmc.h"
#include "dane3.h"


static float Mptab[N * (D - 1)];
static float dUptab[D - 1] = {0};
static float KuTab[D - 1] = {0};
static float Ku_res_;
//float dUtab[Nu] = {0};
//float YTab[N] = {0};
//float Y0Tab[N] = {0};
//float Y0Tab_bis[N] = {0};

static matrix_t K = {
        .width = N,
        .height = Nu,
        .cells = Ktab
};

static matrix_t K1 = {
        .width = N,
        .height = 1,
        .cells = Ktab
};

static matrix_t Ku = {
        .width = D - 1,
        .height = 1,
        .cells = KuTab
};

static matrix_t Ku_res = {
        .width = 1,
        .height = 1,
        .cells = &Ku_res_
};


static matrix_t Mp = {
        .width = D - 1,
        .height = N,
        .cells = Mptab
};

static matrix_t dUp = {
        .width = 1,
        .height = D - 1,
        .cells = dUptab
};

static float Ke;

void dmc_init(void) {

    //init Mp
    for (int row = 1; row <= Mp.height; row++) {
        for (int coll = 1; coll <= Mp.width; coll++) {

            if (row + coll <= D)
                *matrix_access(&Mp, row - 1, coll - 1) = Stab[row + coll - 1] - Stab[coll - 1];
            else
                *matrix_access(&Mp, row - 1, coll - 1) = Stab[D - 1] - Stab[coll - 1];
        }
    }

    Ke = 0.f;
    for (int a = 0; a < K.width; a++) {
        Ke += *matrix_access(&K, 0, a);
    }

    matrix_mult(&K1, &Mp, &Ku);
//    matrix_print(&Mp);
}

/*
 *
 *
 * */
float dmc_go(float y, float yzad, float u_minmax) {

    static float u = 0;
    float e = yzad - y;
    float du;
    float u_old = u;

    int err = 0;

    // Ku * dUp;
    err += matrix_mult(&Ku, &dUp, &Ku_res);

    du = Ke * e - *matrix_access(&Ku_res, 0, 0);

    u = u + du;

    if (u < -u_minmax) {
        u = -u_minmax;
    } else if (u > u_minmax) {
        u = u_minmax;
    }

    //dU make space for new du value
    for (int a = sizeof(dUptab) / sizeof(dUptab[0]) - 1; a >= 1; a--) {
        dUptab[a] = dUptab[a - 1];
    }

    *matrix_access(&dUp, 0, 0) = u - u_old;

    return u;
}



