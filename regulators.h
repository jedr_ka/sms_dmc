//
// Created by jedrek on 2017-12-10.
//

#ifndef UNTITLED_DMC_H
#define UNTITLED_DMC_H

#include <stdint.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "matrix.h"
#include "floatdebug.h"

//#define DMCFAST


#ifndef min
#define min(a, b)            (((a) < (b)) ? (a) : (b))
#endif

void dmc_init();
float dmc_go(float y, float yzad, float u_minmax);

void pid_init();
float pid_go(float y, float yzad, float u_minmax, float Tv);
void pid_see(float *re, float *rup, float *rui, float *rud);


#endif //UNTITLED_DMC_H
