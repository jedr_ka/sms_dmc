//
// Created by jedrek on 2017-12-10.
//

#include "regulators.h"
#include "dane2.h"


float Mptab[N * (D - 1)];
float dUptab[D - 1] = {0};
float dUtab[Nu] = {0};
float YTab[N] = {0};
float Y0Tab[N] = {0};
float Y0Tab_bis[N] = {0};

matrix_t K = {
        .width = N,
        .height = Nu,
        .cells = Ktab
};

matrix_t K1 = {
        .width = N,
        .height = 1,
        .cells = Ktab
};

matrix_t Mp = {
        .width = D - 1,
        .height = N,
        .cells = Mptab
};

matrix_t dUp = {
        .width = 1,
        .height = D - 1,
        .cells = dUptab
};

matrix_t dU = {
        .width = 1,
        .height = Nu,
        .cells = dUtab
};

matrix_t dU1 = {
        .width = 1,
        .height = 1,
        .cells = dUtab
};

matrix_t Y = {
        .width = 1,
        .height = N,
        .cells = YTab
};

matrix_t Y_0 = {
        .width = 1,
        .height = N,
        .cells = Y0Tab
};

matrix_t Y_0_bis = {
        .width = 1,
        .height = N,
        .cells = Y0Tab_bis
};

void dmc_init() {

    //init Mp
    for (int row = 1; row <= Mp.height; row++) {
        for (int coll = 1; coll <= Mp.width; coll++) {

            if (row + coll <= D)
                *matrix_access(&Mp, row - 1, coll - 1) = Stab[row + coll - 1] - Stab[coll - 1];
            else
                *matrix_access(&Mp, row - 1, coll - 1) = Stab[D - 1] - Stab[coll - 1];
        }
    }
    matrix_print(&Mp);
}

/*
 *  Y_0 = y(k)*ones(N,1) + Mp * du(end:-1:end-D+2)';
    deltaU=K*(Yzad(k+1:k+N)'-Y_0);
    du=[du deltaU(1)];
 * */
float dmc_go(float y, float yzad, float u_minmax) {

    static float u = 0;
    int err = 0;

    // Mp * dUp;
    err += matrix_mult(&Mp, &dUp, &Y_0);

    //y(k)*ones(N,1)
    //memset(YTab, y, sizeof(YTab));
    for (int a = 0; a < sizeof(YTab) / sizeof(YTab[0]); a++) {
        YTab[a] = y;
    }

    //Y_0 = y(k)*ones(N,1) + Mp * dUp;
    err += matrix_subadd(&Y, &Y_0, +1.f, &Y_0_bis);

    //Yzad = yzad * ones...
    //memset(YTab, yzad, sizeof(YTab));
    for (int a = 0; a < sizeof(YTab) / sizeof(YTab[0]); a++) {
        YTab[a] = yzad;
    }

    //(Yzad-Y_0)
    err += matrix_subadd(&Y, &Y_0_bis, -1.f, &Y_0);


#ifdef DMCFAST
    err += matrix_mult(&K1, &Y_0, &dU1);
    u = u + *matrix_access(&dU1, 0, 0);
#else
    //deltaU=K*(Yzad-Y_0);
    err += matrix_mult(&K, &Y_0, &dU);
    //u = u + dU[k|k]
    u = u + *matrix_access(&dU, 0, 0);
#endif

    //dU make space for new du value
    for (int a = sizeof(dUptab) / sizeof(dUptab[0]) - 1; a >= 1; a--) {
        dUptab[a] = dUptab[a - 1];
    }

    if (u < -u_minmax) {
        u = -u_minmax;
    } else if (u > u_minmax) {
        u = u_minmax;
    }

#ifdef DMCFAST
    //dUp[0] = dU[k|k];
    *matrix_access(&dUp, 0, 0) = *matrix_access(&dU1, 0, 0);
#else
    //dUp[0] = dU[k|k];
    *matrix_access(&dUp, 0, 0) = *matrix_access(&dU, 0, 0);
#endif


//    for (int a = sizeof(dUptab) / sizeof(dUptab[0]) - 1; a >= 0; a--) {
//       // debug_float(&dUtab[a]);
//    }

    return u;

}



