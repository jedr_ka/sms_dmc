//
// Created by jedrek on 2017-12-10.
//

#include "matrix.h"


float *matrix_access(matrix_t *a, int row, int coll) {
    if (row >= a->height || coll >= a->width) {
        row++;

    }
    return &a->cells[a->width * row + coll];
}

int matrix_mult(matrix_t *ma, matrix_t *mb, matrix_t *result) {
    int err = 0;
    if (ma->width != mb->height) {
        err += 1;
    }


    if (ma->height != result->height || mb->width != result->width) {
        err += 4;
    }
    if (err != 0)
        return err;

    for (int row = 0; row < result->height; row++) {
        for (int coll = 0; coll < result->width; coll++) {

            float rowcollsum = 0.0f;
            for (int a = 0; a < ma->width; a++) {
                rowcollsum += *matrix_access(ma, row, a) * *matrix_access(mb, a, coll);
            }
            *matrix_access(result, row, coll) = rowcollsum;
        }
    }
    return err;
}

int matrix_mult_scalar(matrix_t *ma, float mult, matrix_t *result) {

    int err = 0;

    if (ma->height != result->height || ma->width != result->width) {
        err += 4;
    }

    if (err != 0)
        return err;

    for (int row = 0; row < result->height; row++) {
        for (int coll = 0; coll < result->width; coll++) {
            *matrix_access(result, row, coll) = *matrix_access(ma, row, coll) * mult;
        }
    }
    return err;
}


int matrix_subadd(matrix_t *ma, matrix_t *mb, float par, matrix_t *result) {
    int err = 0;
    if (ma->width != mb->width) {
        err += 1;
    }

    if (ma->height != mb->height) {
        err += 2;
    }

    if (ma->height != result->height || ma->width != result->width) {
        err += 4;
    }
    if (err != 0)
        return err;

    for (int row = 0; row < result->height; row++) {
        for (int coll = 0; coll < result->width; coll++) {
            *matrix_access(result, row, coll) = *matrix_access(ma, row, coll) + par * (*matrix_access(mb, row, coll));
        }
    }
    return err;
}

void matrix_print(matrix_t *m) {
    for (int row = 0; row < m->height; row++) {
        for (int coll = 0; coll < m->width; coll++) {
            printf("%6.4f ", *matrix_access(m, row, coll));
        }
        printf("\n");
    }
    printf("\n");
}
