//
// Created by jedrek on 2017-12-10.
//

#ifndef UNTITLED_PID_H
#define UNTITLED_PID_H

#include <stdint.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "matrix.h"
#include "floatdebug.h"


#ifndef min
#define min(a, b)            (((a) < (b)) ? (a) : (b))
#endif

void pid_init();
float pid_go(float y, float yzad, float u_minmax, float Tv);
void pid_see(float *re, float *rup, float *rui, float *rud);


#endif //UNTITLED_PID_H
