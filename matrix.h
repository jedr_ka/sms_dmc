//
// Created by jedrek on 2017-12-10.
//

#ifndef UNTITLED_MATRIX_H
#define UNTITLED_MATRIX_H


#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

struct matrix_s {
    float *cells;
    uint32_t width;
    uint32_t height;
};
typedef struct matrix_s matrix_t;

float *matrix_access(matrix_t *a, int row, int coll);



/*
 * @param ma first matrix
 * @param mb second martix
 * @param result where to store multiply result
 */
int matrix_mult(matrix_t *ma, matrix_t *mb, matrix_t *result);

int matrix_mult_scalar(matrix_t *ma, float mult, matrix_t *result);

/*
 * @param ma first matrix
 * @param mb second martix
 * @param par -1.f for substracting or 1.f for adding
 * @param result where to store result
 */
int matrix_subadd(matrix_t *ma, matrix_t *mb, float par, matrix_t *result);


void matrix_print(matrix_t * m);


#endif //UNTITLED_MATRIX_H
