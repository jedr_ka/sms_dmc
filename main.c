//#include <iostream>

#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <stdlib.h>
#include "regulators.h"
#include "matrix.h"

//#include "dane2.h"
//#include "dane3.h"
//#include "dane4.h"
//#include "dane5.h"
//#include "dane6.h"
//#include "dane7.h"
//#include "dane8.h"
//#include "dane9.h"
//#include "dane10.h"

//float bwsp[13] = {0.0526f, 0.0588f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f};
//float awsp[13] = {0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.7151f, -1.6941f, 0};
float bwsp[13] = {0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.0101673068632589f, 0.0114382971744303f, 0.f};
float awsp[13] = {0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.702294902567729f, -1.68129018414254f, 0.f};


int main() {


    float mat[] = {1.f, 2.f, 3.f, 4.f, 5.f, 6.f};
    float mbt[] = {11.f, 12.f, 13.f, 14.f, 15.f, 16.f, 17.f, 18.f, 19.f, 20.f, 21.f, 22.f};
    float mct[12];
    float mdt[6];

    matrix_t ma = {
            .cells = mat,
            .width=3,
            .height=2
    };


    matrix_t mb = {
            .cells = mbt,
            .width=4,
            .height=3
    };

    matrix_t mc = {
            .cells = mct,
            .width=4,
            .height=2
    };
    matrix_t md = {
            .cells = mdt,
            .width=3,
            .height=2
    };

    printf("err %d\n", matrix_mult(&ma, &mb, &mc));
    matrix_print(&mc);

    printf("err %d\n", matrix_subadd(&ma, &ma, -2.f, &md));
    matrix_print(&md);
    printf("\n");


    dmc_init();
    pid_init();

    float y[13] = {0.f};
    float u[13] = {0.f};

    for (int a = 0; a < 1000; a++) {

        for (int q = sizeof(y)/sizeof(y[0]) - 1; q >= 1; q--) {
            y[q] = y[q - 1];
            u[q] = u[q - 1];
        }
        //y(k)=u(end-model_si+1:end)*b' - y(end-model_si+1:end) * a'+e(k);
        u[0] = 0.f;
        y[0] = 0.f;

        for (int q = 0; q < sizeof(bwsp)/sizeof(bwsp[0]) -1 ; q++) {

            float  _bwsp= bwsp[q];
            float _u = u[13 - q - 1];
            float _1 = _bwsp * _u;

            float _awsp = awsp[q];
            float _y = y[13 - q - 1];
            float _2 = _awsp * _y;

            y[0] += _1 - _2;
//            y[0] += bwsp[q] * u[13 - q - 1] - awsp[q] * y[13 - q - 1];
        }

//        if (a < 100)
//            u[0] = dmc_go(y[0], 0.f, 2000.f);
//        else
//            u[0] = dmc_go(y[0], 100.f,2000.f);


        if (a < 100)
            u[0] = pid_go(y[0], 0.f, 500.f, 100.f);
        else
            u[0] = pid_go(y[0], 100.f, 500.f, 100.f);

        //u[0]=100;

        printf("i: # %5d # u: # %8.6f #  y: # %8.6f # \n",a,u[0], y[0]);

        if(a==143 || a == 120){
           a++;
        }
    }


    return 0;
}
