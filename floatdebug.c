//
// Created by jedrek on 2017-12-10.
//
#include "floatdebug.h"
void debug_float(float *f) {
    float_cast d1;
    d1.f = *f;

    printf("sign = %x exponent = %d mantisa = %d float = %8.6f \n", d1.parts.sign, d1.parts.exponent,
           d1.parts.mantisa, *f);
}