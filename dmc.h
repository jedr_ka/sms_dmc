#ifndef UNTITLED_DMC_H
#define UNTITLED_DMC_H

#include <stdint.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "matrix.h"
#include "floatdebug.h"


#ifndef min
#define min(a, b)            (((a) < (b)) ? (a) : (b))
#endif

void dmc_init(void);
float dmc_go(float y, float yzad, float u_minmax);

#endif //UNTITLED_DMC_H
